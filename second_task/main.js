console.log('Working')

// First task Javascript
function calculate(){
    let number1 = document.getElementById('number1').value;
    let number2 = document.getElementById('number2').value;
    let operation = document.getElementById('operation').value;
    let result = eval(number1 + operation + number2);
    document.getElementById('answer').innerHTML = result;

}

// second task JavaScript
function get_table(){
    let number = document.getElementById('second_task_number').value;
    let i = 1;
    while(i<11){
        document.getElementById('answer2').innerHTML += number + ' x ' + i + ' = ' + (number * i) + '<br>';
        console.log(i);
        i++;
    }
}

function third_task(){
    var number3 = document.getElementById('third_task_text').value;
    let count = 1
    for(var i in number3){
        if(number3[i] == ' '){
            count += 1;
        }
    }
    document.getElementById('answer3').innerHTML = count;
}

function fourth_task(){
    let number4 = document.getElementById('fourth_task_text').value;
    let exps = {
        'minus': '-',
        'plus': '+',
        'multiply': '*',
        'divided by': '/'
    }
    let keys = Object.keys(exps);
    for(let i in keys){
        if(number4.match(keys[i]) != null){
            let exp_result = number4.replace(keys[i], exps[keys[i]])
            let result = eval(exp_result);
            document.getElementById('answer4').innerHTML = result;
        }
    }
}

function fifth_task(){
    let number5 = document.getElementById('fifth_task_text').value;
    let scores = {
        'A': 1,'E': 1,'I': 1,'O': 1,'U': 1,'L': 1,'N': 1,'R': 1,'S': 1,'T': 1,'D': 2,'G': 2,'B': 3,'C': 3,'M': 3,'P': 3,'F': 4,'H': 4,'V': 4,'W': 4,'Y': 4,'K': 5,'J': 8,'X': 8,'Q': 10,'Z': 10
    }
    let sum = 0;
    number5 = number5.toUpperCase();
    for(let i in number5){
        if(number5[i].toUpperCase() in scores){
            sum += scores[number5[i]];
        }
    }
    document.getElementById('answer5').innerHTML = sum;
}

function sixth_task(){
    var s = document.getElementById('sixth_task_text').value;
    document.getElementById('answer6').innerHTML = nested_braces(s);
    
    
}


function nested_braces(s) {
    const stack = [], opening = ['(', '{', '['], closing = [')', '}', ']'],brackets=opening.concat(closing);
    for (let c of s) {
        if(c.indexOf(brackets) != -1){
            if (opening.indexOf(c) !== -1) stack.push(c);
            else {
                const idx = closing.indexOf(c);
                if (stack[stack.length - 1] === opening[idx]) stack.pop();
                else return false;
            }
        }
    }
    return stack.length === 0;
};



// Seventh task Jquery
class ModArray extends Array{
    chunck(i){
        // console.log(i)
        let div = ~~(this.length/i);
        // console.log(this.length, i, div,this)
        let output_chunck = [];
        for(let i = 0; i < div; i++){
            output_chunck.push(this.slice(0,div));
        }
        return output_chunck;
    }
    concat(...arr){
        // console.log(arr)
        let output_concat = this;
        for(let i of arr){
            for(let j of i){
                output_concat.push(j);
            }
        }
        return output_concat;
    }
    reverse(){
        let output_reverse = [];
        for(let i = this.length-1; i >= 0; i--){
            output_reverse.push(this[i]);
        }
        return output_reverse;
    }
    remove(ele){
        let output_remove = [];
        for(let i of this){
            if(i != ele){
                output_remove.push(i);
            }
        }
        return output_remove;
    }
}
// let k = new ModArray(1,2,3,4,5,6,7,8,9,10)
// console.log(k.remove(2))


// Seventh task

class ModString extends String{
    camelCase(){
        return this.toLowerCase().replace(/[^a-zA-Z0-9]+(.)/g, (a, br) => {
            console.log(a, br)
            return 'br.toUpperCase();'
        });
    }
    count(){
        if(this.length == 0){
            return 0;
        }
        let count = 1;
        for(let i of this){
            if(i == ' '){
                count += 1;
            }
        }
        // console.log(this.match(/[a-zA-Z0-9]+(.)/g))
        return count;
    }
    count_second_method(){
        return this.match(/[a-zA-Z0-9]+(.)/g).length;
    }
    specialChar(){
        return this.match(/[^\w\s]/g).length
    }
    captialize(){
        return this.replace(/\b[a-z]/g, function(a){
            return a.toUpperCase();
        });
    }
    template_replace(data){
        return this.replace(
            /%(\w*)%/g, // or /{(\w*)}/g for "{this} instead of %this%"
            function( m, key ){
              return data.hasOwnProperty( key ) ? data[ key ] : "";
            }
          );
    }
    
    
}

// let test_string = new ModString('hello world@how are< %test% you!!^&#');
// console.log(test_string.template_replace({'test': 'test1'}))
// console.log(/[^a-zA-Z0-9]+(.)/g)


// 12th silde task
function arr_var(arr){
    for(let i=0;i<=10;i++){
        eval(`var arr${i} = arr[${i}]`);
    }
    var remaining_arr = arr.slice(10);
    console.log(arr0, arr1, arr2, arr3, arr4, arr5, arr6, arr7, arr8, arr9, remaining_arr)
}

// arr_var([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,])

// 13th slide task
function calculate(...arr){
    let operation = arr[0]
    let numbers = arr.slice(1)
    // console.log(numbers,operation)
    let eval_op = ''
    for(let i of numbers){
        eval_op += i + operation
    }
    return eval(eval_op.slice(0,eval_op.length-1))
}

// console.log(calculate('+',1,2,3,4,5,6,7,8,9,10))


